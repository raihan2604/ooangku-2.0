from django.db import models

# Create your models here.
class incomemodels(models.Model):
    income = models.IntegerField()
    description = models.CharField(max_length = 10000)
    owner = models.CharField(max_length=60, default='')

class expensemodels(models.Model):
    expense = models.IntegerField()
    description1 = models.CharField(max_length = 10000)
    owner1 = models.CharField(max_length=60, default='')
