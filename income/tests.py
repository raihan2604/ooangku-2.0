from django.test import TestCase, Client
from .models import incomemodels, expensemodels
from . import views
from django.urls import reverse, resolve
from .forms import IncomeForm, ExpenseForm

class income_test(TestCase):

    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_form(self):
        form_data = {
            'income' : 1000000,
            'description' : 'dari mama',
        }
        form_data2 = {
            'expense' : 50000,
            'description' : 'beli boba',
        }
        form = IncomeForm(data = form_data)
        self.assertTrue(form.is_valid())
        form2 = ExpenseForm(data = form_data2)
        self.assertFalse(form2.is_valid())

    def test_createIncome(self, description = "gaji", income = 10000000):
        return incomemodels.objects.create(income = income, description = description)

    def test_createExpense(self, description = "belanja", income = 5000000):
        return expensemodels.objects.create(expense = income, description1 = description)

    def test_input(self):
        p = self.test_createIncome()
        p2 = self.test_createExpense()
        self.assertEqual(p.income, 10000000)
        self.assertEqual(p.description, "gaji")
        self.assertEqual(p2.expense, 5000000)
        self.assertEqual(p2.description1, "belanja")
