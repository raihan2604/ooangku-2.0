from django.shortcuts import render, redirect
from .models import incomemodels, expensemodels
from .forms import IncomeForm, ExpenseForm
from financialPlanner.models import Account
from django.http import JsonResponse 

def index(request):
    if request.user.is_authenticated:
        obj = expensemodels.objects.filter(owner1 = request.user.email)
        obj2 = incomemodels.objects.filter(owner = request.user.email)
        form1 = IncomeForm()
        form2 = ExpenseForm()
        balance = 0

        u = Account.objects.get(username = request.user.username)

        for a in obj:
            balance -= a.expense

        for b in obj2:
            balance += b.income

        u.total_in_ex = balance
        u.save()

        balance_str = '{:0,.0f}'.format(u.total_in_ex)


        arg = {
            'balance' : balance_str,
            'income' : obj2,
            'expense' : obj,
            'form1' : form1,
            'form2' : form2
        }
        
        return render(request, 'home_income.html', arg)

def income(request):

    response_data = {}
    # if request.POST.get('action') == 'post':
    print("masuk")
    income = int(request.POST.get('income'))
    desc = request.POST.get('description')
    u = Account.objects.get(username = request.user.username)
    u.total_in_ex+=income
    u.save()

    baru = incomemodels.objects.create(
        income = income,
        description = desc,
        owner = request.user.email
    )

    response_data['income'] = str(income)
    response_data['desc']= desc
    response_data['total']= u.total_in_ex
    response_data['id']=baru.id
    return JsonResponse(response_data)
            
    # return redirect('records:index') 


def expense(request):

    response_data = {}
    # if request.POST.get('action') == 'post':
    print("masuk")
    expense = int(request.POST.get('expense'))
    desc = request.POST.get('description')
    print(desc)
    u = Account.objects.get(username = request.user.username)
    u.total_in_ex-=expense
    u.save()

    baru = expensemodels.objects.create(
        expense = expense,
        description1 = desc,
        owner1 = request.user.email
    )

    response_data['expense'] = str(expense)
    response_data['desc']= desc
    response_data['total']= u.total_in_ex
    response_data['id']=baru.id
    return JsonResponse(response_data)

def income_delete(request,id):
    obj1 = incomemodels.objects.get(id=id)
    u = Account.objects.get(username = request.user.username)
    u.total_in_ex-=obj1.income
    obj1.delete()
    return redirect('/income/')

def expense_delete(request,id):
    obj2 = expensemodels.objects.get(id=id)
    u = Account.objects.get(username = request.user.username)
    u.total_in_ex+=obj2.expense
    obj2.delete()
    return redirect('/income/')

