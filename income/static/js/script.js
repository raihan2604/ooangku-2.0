$(document).ready(function () {
    console.log("detected");

    $('.form1').on('submit',function(e){
        console.log("found");
        e.preventDefault()
        $.ajax({
            type:'POST',
            url:"/income/incomes",
            data:{
                income:$('#id_income').val(),
                description:$('#id_description').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                action: 'post'
            },
            success:function(json){
                console.log("sukses");
                $(".table-income").append(
                    "<tr>"+
                    "<td style='width: 50%'>"+json.desc+"</td>"+
                    "<td style='width: 10%'>"+json.income+"</td>"+
                    "<td style='width:10%'>"+
                    "<a href = 'delete/"+json.id+"'><button class = 'btn btn-outline-dark'>Delete</button></a>"+
                    "</td>"+
                    "</tr>"
                );
                $("#balance").empty();
                $("#balance").append(
                    "<p>IDR</p>"+
                    "<p>"+json.total+"</p>"
                );
                $(".close").trigger('click');
            },
            error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
        });
    });

    $('.form2').on('submit',function(e){
        console.log("found");
        e.preventDefault()
        $.ajax({
            type:'POST',
            url:"/income/expenses",
            data:{
                expense:$('#id_expense').val(),
                description:$('#id_description1').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                action: 'post'
            },
            success:function(json){
                console.log("sukses");
                console.log(json.desc);
                $(".table-outcome").append(
                    "<tr>"+
                    "<td style='width: 50%'>"+json.desc+"</td>"+
                    "<td style='width: 10%'>"+json.expense+"</td>"+
                    "<td style='width:10%'>"+
                    "<a href = 'delete1/"+json.id+"'><button class = 'btn btn-outline-dark'>Delete</button></a>"+
                    "</td>"+
                    "</tr>"
                );
                $("#balance").empty();
                $("#balance").append(
                    "<p>IDR</p>"+
                    "<p>"+json.total+"</p>"
                );
                $(".close").trigger('click');
            },
            error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
        });
    });

    $("#tambah-income").onmouseover(function(){
        $(this).removeClass("btn-warning");
        $(this).addClass("btn-primary");
    });
})