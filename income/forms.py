from django import forms
from django.forms import widgets
from .models import incomemodels, expensemodels

class IncomeForm(forms.ModelForm):
    class Meta:
        model = incomemodels
        fields = '__all__'
        widgets = {
        'income' : forms.NumberInput(attrs={'class': 'form-control'}),
        'description' : forms.TextInput(attrs={'class': 'form-control'}),
        }
        exclude = ['owner']

class ExpenseForm(forms.ModelForm):
    class Meta:
        model = expensemodels
        fields = '__all__'
        widgets = {
        'expense' : forms.NumberInput(attrs={'class': 'form-control'}),
        'description1' : forms.TextInput(attrs={'class': 'form-control'}),
        }
        exclude = ['owner1']