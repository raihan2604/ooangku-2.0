# TUGAS KELOMPOK 2 PPW

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/rabialco/story-6/badges/master/pipeline.svg)](https://gitlab.com/raihan2604/ooangku-2.0/commits/master)
[![coverage report](https://gitlab.com/rabialco/story-6/badges/master/coverage.svg)](https://gitlab.com/raihan2604/ooangku-2.0/commits/master)

## URL
This lab projects can be accessed from [https://mraihan26.herokuapp.com](https://ooangku.herokuapp.com)

## Author
**Muhammad Raihan** - [raihan2604](https://gitlab.com/raihan2604)
**Bhaskoro**
**Abi Fajri**
**Wulan**
