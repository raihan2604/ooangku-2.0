from django.shortcuts import render, redirect
from django.http import HttpResponse
from financialPlanner.models import Account
from django.contrib.auth import login, authenticate
from .forms import RegistrationForm
from budgeting.models import Budgeting

# Create your views here.

def index(request):
	if request.user.is_authenticated:
		u = Account.objects.get(email = request.user.email)

		# total_budgets = 0
		# budgets = Budgeting.objects.filter(owner = request.user.email)
		# for budget in budgets:
		# 	total_budgets += budget.value
		# total_budgets_str = '{:0,.0f}'.format(total_budgets)

		# u.budget = total_budgets_str
		# u.save()

		print(u.perYear)
		return render(request, "index.html", {'pemasukan':request.user.perYear, 'utang':request.user.liability})
	else:
		return render(request, "index.html")

def registration_view(request):
	context = {}
	if request.POST:
		form = RegistrationForm(request.POST)
		if form.is_valid():
			form.save()
			email = form.cleaned_data.get('email')
			raw_password = form.cleaned_data.get('password1')
			account = authenticate(email=email, password=raw_password)
			login(request, account)
			return redirect('/')
		else:
			context['registration_form'] = form
	else: #GET request
		form = RegistrationForm()
		context['registration_form'] = form
	return render(request, 'register.html', context)
