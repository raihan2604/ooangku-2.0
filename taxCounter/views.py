from django.shortcuts import render, redirect
from .models import TaxModels
from .forms import TaxForm
from financialPlanner.models import Account
from django.http import JsonResponse

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        u = Account.objects.get(email = request.user.email)
        response_data = {}
        form = TaxForm()

        if request.POST.get('action') == 'post':
            TaxModels.objects.all().delete()
            gaje = request.POST.get('income')
            income = int(gaje)
            married = request.POST.get('married')
            kids = request.POST.get('kids')
            jumlahAnak = int(kids)
            u = Account.objects.get(username = request.user.username)
            print("kuy")

            print(u.username)
            u.perYear = income
            pkp = hitungpkp(income, married, jumlahAnak)
            pajak = hitungPajak(pkp)
            u.taxAmount = pajak
            u.save()
            pajak = str(pajak)   

            TaxModels.objects.create(
                income = gaje,
                married = married,
                kids = kids,
            )
            print("success")
            return JsonResponse({'foo':u.taxAmount})
        return render(request, 'home_tax.html', {'form':form , 'pajak':u.taxAmount})

        # if request.method == 'POST':
        #     form = TaxForm(request.POST)
        #     if form.is_valid():
        #         form.save()
        #         obj = TaxModels.objects.all().last()
        #         gaji = obj.income
        #         nikah = obj.married
        #         anak = int(obj.kids)
        #         u = Account.objects.get(username=request.user.username)
        #         u.perYear = gaji
        #         pkp = hitungpkp(gaji, nikah, anak)
        #         pajak = hitungPajak(pkp)
        #         u.taxAmount = pajak
        #         pajak = str(pajak)
        #         u.save()
                
        #         args = {'form' : form,'pajak':u.taxAmount}
        #         TaxModels.objects.all().delete()
        #         return render(request, 'home_tax.html', args)
        # else:
        #     form = TaxForm()
        #     u = Account.objects.get(email = request.user.email)
        #     print(u.perYear)
        # return render(request, 'home_tax.html', {'form':form, 'pajak':u.taxAmount})
        # form = TaxForm()
        # u = Account.objects.get(email = request.user.email)
        # return render(request, 'home_tax.html', {'pajak':u.taxAmount, 'form':form})
    else:
        return render(request, 'home_tax.html')

def hitungpkp(income, married, kids):
    ptkp = 54000000
    ptkp_istriAnak = 4500000
    if(married == 'yes'):
        ptkp += ptkp_istriAnak
    jumlahAnak = int(kids)
    ptkp += jumlahAnak*ptkp_istriAnak
    return income-ptkp
    

def hitungPajak(pkp):
    if(pkp<50000000):
        return 5/100*pkp
    elif(pkp<250000000):
        pajak = 15/100*(pkp-50000000)
        return pajak+2500000
    elif(pkp<500000000):
        pajak=  25/100*(pkp-250000000)
        return pajak+32500000
    else:
        pajak = 30/100*(pkp-500000000)
        return pajak+95000000


def pajakBos(request):
    TaxModels.objects.all().delete()
    response_data = {}

    if request.POST.get('action') == 'post':
        income = int(request.POST.get('income'))
        married = request.POST.get('married')
        print(married)
        kids = request.POST.get('kids')
        jumlahAnak = int(kids)
        u = Account.objects.get(username = request.user.username)
        u.perYear = income
        pkp = hitungpkp(income, married, jumlahAnak)
        pajak = hitungPajak(pkp)
        u.taxAmount = pajak
        pajak = str(pajak)
        response_data['pajak'] = pajak

        TaxModels.objects.create(
            income = income,
            married = married,
            kids = kids,
        )
        return JsonResponse(response_data) 
