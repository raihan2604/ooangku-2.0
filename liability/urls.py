from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('input', views.input, name='input'),
    path('modify/<int:id>/', views.modify, name='modify'),
    path('paid/<int:id>/', views.paid, name='paid')

]
