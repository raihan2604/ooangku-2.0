$(document).ready(function () {
    console.log("detected");
    
    $('#post-form').on('submit',function(e){
        e.preventDefault()
        $.ajax({
            type:'POST',
            url:'/liability/',
            data:{
                nama:$('#id_nama').val(),
                duedate:$('#id_duedate').val(),
                value:$("#id_value").val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                action: 'post'
            },
            success:function(json){
                var hasil = "";
                $("#total-liability").empty();
                $("#total-liability").append(
                    "<p>IDR " + json.total_utang + "</p>"
                )

                hasil+= (
                    '<tr> <td>' + json.nama + '</td>' +
                    '<td>' + json.duedate + '</td>' +
                    '<td>' + json.value + '<td>' +
                    '</tr>'
                )
                console.log(hasil)

                $("#tabel").append(hasil);


                $(".close").trigger('click');
            },
            error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
        });
    });
})