from django.test import TestCase, Client
from .models import liabilitymodels
from . import views
from django.urls import reverse, resolve
from .forms import *
from django.contrib.auth import login, authenticate

class liability_test(TestCase):

    def test_url_liabilities(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_url1(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def createLiability(self, nama="tes", duedate='2000-10-20', value=1000):
        return liabilitymodels.objects.create(nama=nama, duedate = duedate, value = value)

    def test_views(self):
        handler = resolve('/liability/')
        self.assertEqual(handler.func, views.index)
    
    def test_input(self):
        p = self.createLiability()
        self.assertEqual(p.nama,"tes")
        self.assertEqual(p.duedate, '2000-10-20')
        self.assertEqual(p.value, 1000)

    def test_form(self):
        form_data = {
            'Nama': 'tes',
            'duedate':'2000-10-20',
            'value':1000
        }
        form = LiabilityForm(data = form_data)
        self.assertFalse(form.is_valid())

    # def test_views_input(self):


    
        
# Create your tests here.
