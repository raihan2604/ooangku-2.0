from django import forms
from django.forms import widgets
from .models import liabilitymodels

class CustomDateInput(widgets.TextInput):
    input_type = 'date'

class LiabilityForm(forms.ModelForm):
    class Meta:
        model = liabilitymodels
        fields = {'nama','value','duedate'}
        widgets ={
            'duedate' : CustomDateInput
        }
        exclude = ['pemilik']