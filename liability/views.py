from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import liabilitymodels
from .forms import LiabilityForm
from financialPlanner.models import Account
from django.contrib.auth import login, authenticate

# Create your views here.
def index(request):
    if request.method =='POST':
        utang = liabilitymodels.objects.filter(pemilik = request.user.email)

        response_data = {}
        total_utang = 0

        # if request.POST.get('action') == 'post':
        nama = request.POST.get('nama')
        duedate = request.POST.get('duedate')
        value = request.POST.get('value')

        for x in utang:
            total_utang+=x.value

        total_utang += int(value)
        
        utang_format = '{:0,.0f}'.format(total_utang)

        response_data['nama'] = str(nama)
        response_data['duedate'] = duedate
        response_data['value'] = value
        response_data['total_utang'] = utang_format


        a = liabilitymodels.objects.create(
            nama = nama,
            duedate = duedate,
            value = value,
            pemilik = request.user.email
            )
        a.save()
        return JsonResponse(response_data)


    obj = liabilitymodels.objects.filter(pemilik = request.user.email)
    form = LiabilityForm()
    utang = 0
    for x in obj:
        utang+=x.value
        print(x)
    utang_format = '{:0,.0f}'.format(utang)

    u = Account.objects.get(email = request.user.email)
    u.liability = utang_format
    u.save()

    print(u.liability)

    return render(request, "home_liability.html", {'form':form,'utang':utang_format, 'obj':obj})

def paid(request, id):
    all = liabilitymodels.objects.get(id=id)
    all.delete()
    return redirect('/liability/')

def modify(request, id):
    all = liabilitymodels.objects.get(id=id)

def input(request):
    if request.method == 'POST':

        form = LiabilityForm(request.POST)
        if form.is_valid():
            a = form.save(commit = False)
            a.pemilik = request.user.email
            a.save()
            obj = liabilitymodels.objects.filter(pemilik = request.user.email)
            total = 0
            for utang in obj:
                total+=utang.value
            u = Account.objects.get(email = request.user.email)
            u.liability = total
            u.save()

            print(u.liability) 
            # request.session['utang'] = total
            return redirect('/liability/')
    else:
        form = LiabilityForm()
        return render(request, "home_liability.html", {'form':form})


def modify(request, id):
    if request.method == 'POST':
        form = LiabilityForm(request.POST)
        if form.is_valid():
            x = liabilitymodels.objects.get(id=id)
            print(x)
            x.nama = form.cleaned_data['nama']
            x.value = form.cleaned_data['value']
            x.duedate = form.cleaned_data['duedate']
            x.save()
            return redirect('/liability/')
    else:
        form = LiabilityForm()
    return render(request,'modify.html', {'form':form})



