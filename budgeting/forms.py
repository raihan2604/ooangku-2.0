from django import forms
from .models import Budgeting

class BudgetingForm(forms.ModelForm):
    class Meta:
        model = Budgeting
        fields = '__all__'
        widgets = {
            'description' : forms.TextInput(attrs={'class': 'form-control'}),
            'value' : forms.NumberInput(attrs={'class': 'form-control'}),
            'month' : forms.Select(attrs={'class': 'form-control'}),
        }
        exclude = ['owner']
