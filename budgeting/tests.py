from django.test import TestCase, Client
from .models import Budgeting
from . import views
from django.urls import reverse, resolve
from .forms import BudgetingForm

# Create your tests here.
class BudgetingTest(TestCase):
    def budgeting_url_is_exist(self):
        response = self.client.get('/budgeting/')
        self.assertEqual(response.status_code, 200)
    
    def budgeting_using_budgeting_template(self):
        response = self.client.get('/budgeting/')
        self.assertTemplateUsed(response, 'home_budgeting.html')

    def budgeting_using_index_func(self):
        found = resolve('/budgeting/')
        self.assertEqual(found.func, views.index)

    def create_budget(self, desc = 'transport', val = 1000000, mon = 1, own = 'none'):
        return Budgeting.objects.create(description = desc, value = val, month = mon, owner = own)

    def test_input(self):
        budget = self.create_budget()
        self.assertEqual(budget.description, 'transport')
        self.assertEqual(budget.value, 1000000)
        self.assertEqual(budget.month, 1)
        self.assertEqual(budget.owner, 'none')

    def test_form(self):
        form_data = {
            'description' : 'uang makan',
            'value' : 5000000,
            'month' : 1,
            'owner' : 'none',
        }
        form = BudgetingForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_kosong(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
