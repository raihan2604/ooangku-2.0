from django.shortcuts import render, redirect
from .models import Budgeting
from .forms import BudgetingForm
from financialPlanner.models import Account
from django.utils import timezone
from django.http import JsonResponse

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        budgets = Budgeting.objects.filter(owner = request.user.email)
        total_data = Budgeting.objects.filter(owner = request.user.email).count()
        
        january = Budgeting.objects.filter(owner = request.user.email, month=1)
        february = Budgeting.objects.filter(owner = request.user.email, month=2)
        march = Budgeting.objects.filter(owner = request.user.email, month=3)
        april = Budgeting.objects.filter(owner = request.user.email, month=4)
        may = Budgeting.objects.filter(owner = request.user.email, month=5)
        june = Budgeting.objects.filter(owner = request.user.email, month=6)
        july = Budgeting.objects.filter(owner = request.user.email, month=7)
        august = Budgeting.objects.filter(owner = request.user.email, month=8)
        september = Budgeting.objects.filter(owner = request.user.email, month=9)
        october = Budgeting.objects.filter(owner = request.user.email, month=10)
        november = Budgeting.objects.filter(owner = request.user.email, month=11)
        december = Budgeting.objects.filter(owner = request.user.email, month=12)

        months = [1,2,3,4,5,6,7,8,9,10,11,12]

        total_budgets = 0
        for budget in budgets:
            total_budgets += budget.value
        total_budgets_str = '{:0,.0f}'.format(total_budgets)

        response = {
            'form' : BudgetingForm,
            'budgets' : budgets,
            'months' : months,
            'total_budgets' : total_budgets_str,
            'january' : january,
            'february' : february,
            'march' : march,
            'april' : april,
            'may' : may,
            'june' : june,
            'july' : july,
            'august' : august,
            'september' : september,
            'october' : october,
            'november' : november,
            'december' : december,
        }


        response_data = {}
        form = BudgetingForm(request.POST or None)
        if request.POST.get('action') == 'post':
            description = request.POST.get('description')
            value = request.POST.get('value')
            month = request.POST.get('month')

            total_budgets += int(value)
            total_budgets_str = '{:0,.0f}'.format(total_budgets)
            total_data += 1
            
            u = Account.objects.get(email = request.user.email)
            u.budget = total_budgets_str
            u.save()

            response_data['description'] = description
            response_data['value'] = value
            response_data['month'] = month
            response_data['total_budgets'] = total_budgets_str
            response_data['id'] = total_data

            Budgeting.objects.create(
                description = description,
                value = value,
                month = month,
                owner = request.user.email
            )

            return JsonResponse(response_data)

        return render(request, "home_budgeting.html", response)
        
    return render(request, 'home_budgeting.html')

def delete(request, delete_id):
   Budgeting.objects.filter(id=delete_id).delete()
   return redirect('budgeting:index')