$(document).ready(function () {
    console.log("detected");
    $('#form-budgeting').on('submit',function(e) {
        e.preventDefault()
        $.ajax({
            type:'POST',
            url:'/budgeting/',
            data:{
                description:$('#id_description').val(),
                value:$('#id_value').val(),
                month:$('#id_month').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                action: 'post'
            },
            success:function(json){
                console.log("sukses");
                console.log(json.total_budgets);
                
                document.getElementById("form-budgeting").reset();
                var hasil = "";
                if (json.month == 1) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);

                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="#" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#january").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 2) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#february").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 3) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#march").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 4) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#april").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 5) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#may").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 6) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#june").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 7) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#july").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 8) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#august").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 9) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#september").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 10) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#october").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 11) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#november").append(hasil);
                    $(".close").trigger('click');
                } else if (json.month == 12) {
                    $("#total_budget").empty();
                    $("#total_budget").append("IDR " + json.total_budgets);
                    
                    hasil += (
                        `<tr>
                            <td class="text-tb">` + json.description + `</td>
                            <td class="text-tb">IDR ` + json.value + `</td>
                            <td><a href="{% url 'budgeting:delete' ` + json.id + ` %}" class="btn btn-sm btn-outline btn-warning text-white text-tb" style="font-size: 16px">Delete</a></td>
                        </tr>`
                    );
    
                    $("#desember").append(hasil);
                    $(".close").trigger('click');
                }
            },
            error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
        });
    });
});